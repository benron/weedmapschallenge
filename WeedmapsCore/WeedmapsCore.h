//
//  WeedmapsCore.h
//  WeedmapsCore
//
//  Created by Benjamin Pilcher on 3/1/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for WeedmapsCore.
FOUNDATION_EXPORT double WeedmapsCoreVersionNumber;

//! Project version string for WeedmapsCore.
FOUNDATION_EXPORT const unsigned char WeedmapsCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WeedmapsCore/PublicHeader.h>


