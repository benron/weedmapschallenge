//
//  Service.swift
//  WeedmapsCore
//
//  Created by Benjamin Pilcher on 3/1/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//


import Alamofire

public protocol Service {
    init(config: APIConfig)
    var sessionManager: Alamofire.SessionManager { get }
    func displayError(from error: Error?, response: HTTPURLResponse?) -> NSError
}

public class YelpService: Service { // todo: hide behind a protocol
    private let config: APIConfig
    public required init(config: APIConfig) {
        self.config = config
        self.sessionManager = SessionManager() // todo: fix- needs to be injected? singleton? 
    }
    
    public var sessionManager: SessionManager
    
    public func displayError(from error: Error?, response: HTTPURLResponse?) -> NSError {
        fatalError()
    }
    
    public func search(for term: String?, searchLocation: SearchLocation, offset: Int, limit: Int, attributes: [String] = [], completion: @escaping ((Result<[Business]>) -> Void)) {
        let endpoint = config.resourceURL(Yelp.searchBusinesses.path)
        var urlComponents = URLComponents(string: endpoint.absoluteString)!
        
        var queryItems: [URLQueryItem] = []
        queryItems.append(URLQueryItem(name: "offset", value: String(offset)))
        queryItems.append(URLQueryItem(name: "limit", value: String(limit)))
        
        if let searchTerm = term {
            queryItems.append(URLQueryItem(name: "term", value: searchTerm))
        }
        
        if !attributes.isEmpty {
             queryItems.append(URLQueryItem(name: "attributes", value: attributes.joined(separator: ",")))
        }
        
        switch searchLocation {
        case .coordinates(let lat, let lon):
            queryItems.append(URLQueryItem(name: "latitude", value: String(lat)))
            queryItems.append(URLQueryItem(name: "longitude", value: String(lon)))
        case .location(let value):
            queryItems.append(URLQueryItem(name: "location", value: value))
        }

        urlComponents.queryItems = queryItems
        
        let headers = [
            "Authorization":"Bearer dv1DC3rl59TJ7MxW0A9ztFmandZwtIFfNpigrdGfWufvPbgSYBFHG8KXF4YGY8Bmdk1YgBfvqyDGiVDwO92US3QYq_Y9if18QkKrwWmDXfcaLpC00xYV2PyJibx5XHYx",
            ]
        
        // Fetch Request
        let url = urlComponents.url!
        Alamofire.request(url, method: .get, parameters: nil, headers: headers)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                if (response.result.error == nil) {
                    debugPrint("HTTP Response Body: \(String(describing: response.data))")
                    let decoder = JSONDecoder()
                    
                    do {
                        let results = try decoder.decode(BusinessResults.self, from: response.data!)
                        completion(.success(results.businesses))
                    } catch let error {
                        print(error)
                    }
                }
                else {
                    debugPrint("HTTP Request failed: \(response.result.error.debugDescription))")
                }
        }
        
        print("search: \(urlComponents.description)")
    }
}

public enum Result<Value> {
    case success(Value)
    case failure(Error)
}

public enum SearchLocation {
    case location(value: String)
    case coordinates(lat: Double, lon: Double)
}

