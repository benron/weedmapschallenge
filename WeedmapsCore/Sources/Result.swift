//
//  Result.swift
//  WeedmapsCore
//
//  Created by Benjamin Pilcher on 3/5/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import Foundation

public enum Result<Value> {
    case success(Value)
    case failure(Error)
}
