//
//  Copyright © 2018 Weedmaps, LLC. All rights reserved.
//

import Foundation

public struct Business: Codable {
    public let id: String
    public let alias: String
    public let name: String
    public let imageURL: String
    public let isClosed: Bool
    public let url: String
    public let reviewCount: Int
    public let categories: [Category]
    public let rating: Float
    public let price: String?
    public let location: Location?
    public let phone: String
    public let displayPhone: String
    public let distance: Double
    public let coordinates: Coordinate
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case alias
        case name
        case imageURL = "image_url"
        case isClosed = "is_closed"
        case url
        case reviewCount = "review_count"
        case categories
        case rating
        case price
        case location
        case phone
        case displayPhone = "display_phone"
        case distance
        case coordinates
    }
}

public class Location: Codable {
    let address1: String?
    let address2: String?
    let address3: String?
    let city: String
    let zipCode: String
    let country: String
    let state: String
    let displayAddress: [String]
    
    enum CodingKeys: String, CodingKey {
        case address1
        case address2
        case address3
        case city
        case zipCode = "zip_code"
        case country
        case state
        case displayAddress = "display_address"
    }
}

public class Coordinate: Codable, Equatable {
    let latitude: Double
    let longitude: Double
    
    init(coordinates: (latitude: Double, longitude: Double)) {
        self.latitude = coordinates.0
        self.longitude = coordinates.1
    }
    
    public static func == (lhs: Coordinate, rhs: Coordinate) -> Bool {
        return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
    }
}

public class Category: Codable, Equatable {
    
    let alias: String
    let title: String
    
    init(alias: String, title: String) {
        self.alias = alias
        self.title = title
    }
    
    public static func == (lhs: Category, rhs: Category) -> Bool {
        return lhs.alias == rhs.alias && lhs.title == rhs.title
    }
}

class BusinessResults: Codable {
    let businesses: [Business]
    let total: Int
}
