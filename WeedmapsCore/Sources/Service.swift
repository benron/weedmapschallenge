//
//  Service.swift
//  WeedmapsCore
//
//  Created by Benjamin Pilcher on 3/1/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import Foundation
import Alamofire

public protocol Service {
    var sessionManager: Alamofire.SessionManager { get }
    func displayError(from error: Error?, response: HTTPURLResponse?) -> NSError
}

public protocol HTTPClient {
    func request(handler: @escaping ((Data?)-> ()), url: URLConvertible, method: HTTPMethod, parameters: Parameters?)
}

public class AlamofireHTTPClient: HTTPClient {
    
    static let bearerToken = "dv1DC3rl59TJ7MxW0A9ztFmandZwtIFfNpigrdGfWufvPbgSYBFHG8KXF4YGY8Bmdk1YgBfvqyDGiVDwO92US3QYq_Y9if18QkKrwWmDXfcaLpC00xYV2PyJibx5XHYx"
    
    public init() {
        
    }
    
    public func request(handler: @escaping ((Data?)-> ()), url: URLConvertible, method: HTTPMethod, parameters: Parameters?) {
        let headers = [
            "Authorization":"Bearer \(AlamofireHTTPClient.bearerToken)",
            ]
        
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                if (response.result.error == nil) {
                    handler(response.data)
                }
                else {
                    debugPrint("HTTP Request failed: \(response.result.error.debugDescription))")
                }
        }
    }
}

public class YelpService: Service { // todo: hide behind a protocol
    
    private let config: APIConfig
    private let httpClient: HTTPClient
    
    public required init(config: APIConfig, httpClient: HTTPClient = AlamofireHTTPClient()) {
        self.config = config
        self.sessionManager = SessionManager() // todo: fix- needs to be injected? singleton?
        self.httpClient = httpClient
    }
    
    public var sessionManager: SessionManager
    
    public func displayError(from error: Error?, response: HTTPURLResponse?) -> NSError {
        fatalError()
    }
    
    public func search(for term: String?, searchLocation: SearchLocation, offset: Int, limit: Int, attributes: [String] = [], completion: @escaping ((Result<[Business]>) -> Void)) {
        let endpoint = config.resourceURL(Yelp.searchBusinesses.path)
        var urlComponents = URLComponents(string: endpoint.absoluteString)!
        
        var queryItems: [URLQueryItem] = []
        queryItems.append(URLQueryItem(name: "offset", value: String(offset)))
        queryItems.append(URLQueryItem(name: "limit", value: String(limit)))
        
        if let searchTerm = term {
            queryItems.append(URLQueryItem(name: "term", value: searchTerm))
        }
        
        if !attributes.isEmpty {
             queryItems.append(URLQueryItem(name: "attributes", value: attributes.joined(separator: ",")))
        }
        
        switch searchLocation {
        case .coordinates(let lat, let lon):
            queryItems.append(URLQueryItem(name: "latitude", value: String(lat)))
            queryItems.append(URLQueryItem(name: "longitude", value: String(lon)))
        case .location(let value):
            queryItems.append(URLQueryItem(name: "location", value: value))
        }

        urlComponents.queryItems = queryItems
        
        let url = urlComponents.url!
        _ = httpClient.request(handler: { data in
            let decoder = JSONDecoder()
            
            do {
                let results = try decoder.decode(BusinessResults.self, from: data!)
                completion(.success(results.businesses))
            } catch let error {
                print(error)
            }
        }, url: url, method: .get, parameters: nil)
    }
}

