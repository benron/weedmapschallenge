//
//  APIConfig.swift
//  WeedmapsCore
//
//  Created by Benjamin Pilcher on 3/1/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import Foundation

public protocol APIConfig {
    var baseURLPath: String { get }
    func resourceURL(_ path: String) -> URL
}

extension APIConfig {
    public func resourceURL(_ path: String) -> URL {
        guard let baseURL = URL(string: baseURLPath) else {
            fatalError("Received invalid path")
        }
        let fullURL = baseURL.appendingPathComponent(path.description)
        return fullURL
    }
}
