//
//  SearchLocation.swift
//  WeedmapsCore
//
//  Created by Benjamin Pilcher on 3/5/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import Foundation

public enum SearchLocation {
    case location(value: String)
    case coordinates(lat: Double, lon: Double)
}
