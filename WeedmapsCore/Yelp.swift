//
//  APIEndpoint.swift
//  WeedmapsCore
//
//  Created by Benjamin Pilcher on 3/1/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import Foundation

public protocol Path {
    var path: String { get }
}

public typealias Body = [String: Any]

public enum Yelp {
    
    case searchBusinesses
    case viewBusiness(id: Int)
    
    public static let decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(Yelp.iso8601Full)
        return decoder
    }()
    
    public static let encoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(Yelp.iso8601Full)
        return encoder
    }()
    
    public static let iso8601Full: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        return formatter
    }()
    
    public static let errorDomain = "com.weedmaps.yelp"
}

extension Yelp: Path {
    public var path: String {
        var value: String
        switch self {
        case .searchBusinesses:
            value = "businesses/search"
        case .viewBusiness(let ID):
            value = "businesses/\(ID)"
        }
        
        return value
    }
}
