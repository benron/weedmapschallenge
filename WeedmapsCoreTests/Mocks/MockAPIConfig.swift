//
//  MockAPIConfig.swift
//  WeedmapsCoreTests
//
//  Created by Benjamin Pilcher on 3/5/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

@testable import WeedmapsCore

class MockAPIConfig: APIConfig {
    var stubbedBaseURLPath: String!
    
    var baseURLPath: String { return stubbedBaseURLPath }
}
