//
//  MockHTTPClient.swift
//  WeedmapsCoreTests
//
//  Created by Benjamin Pilcher on 3/5/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import Alamofire
@testable import WeedmapsCore

class MockHTTPClient: HTTPClient {
    
    var stubbedData: Data!
    var calledURL: URL!
    
    init(stubbedData: Data) {
        self.stubbedData = stubbedData
    }
    
    func request(handler: @escaping ((Data?)-> ()), url: URLConvertible, method: Alamofire.HTTPMethod, parameters: Parameters?) {
        calledURL = try! url.asURL()
        handler(stubbedData)
    }
    
}
