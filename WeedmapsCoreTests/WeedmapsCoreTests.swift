//
//  WeedmapsCoreTests.swift
//  WeedmapsCoreTests
//
//  Created by Benjamin Pilcher on 3/1/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import XCTest
import Alamofire
@testable import WeedmapsCore

class WeedmapsCoreTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testBusinessModelDecodedFromJsonYieldsExpectedResult() {
        // arrange
        let content = defaultBusiness(name: "Bob's Donuts")
        let data = content.data(using: String.Encoding.utf8, allowLossyConversion: true)!
        let sut = Yelp.decoder
        var business: Business!
        
        // act
        do {
            business = try sut.decode(Business.self, from: data)
        } catch {
            XCTFail()
        }
     
        assertIsCorrect(business: business)
    }
    
    func testYelpAPIRequestYieldsExpectedResult() {
        // arrange
        let content = defaultBusinessesResult(name: "Bob's Donuts")
        let data = content.data(using: String.Encoding.utf8, allowLossyConversion: true)!
        let mockHTTPClient = MockHTTPClient(stubbedData: data)
        let mockAPIConfig = MockAPIConfig()
        mockAPIConfig.stubbedBaseURLPath = "http://www.loremipsum.com/api/v2/"
        let sut = YelpService(config: mockAPIConfig, httpClient: mockHTTPClient)
        
        // act
        sut.search(for: nil, searchLocation: SearchLocation.location(value: "Genoa"), offset: 0, limit: 15, attributes: []) { result in
            switch result {
            case .success(let businesses):
                
                // assert
                self.assertIsCorrect(business: businesses[0])
                self.assertCorrectURLQueryItems(url: mockHTTPClient.calledURL)
            case .failure:
                XCTFail()
            }
        }
    }
    
    func assertCorrectURLQueryItems(url: URL) {
        var urlComponents = URLComponents(string: url.absoluteString)!
        let queryItems = urlComponents.queryItems!
        
        XCTAssert(queryItems[0].name == "offset")
        XCTAssert(queryItems[0].value! == "0")
        XCTAssert(queryItems[1].name == "limit")
        XCTAssert(queryItems[1].value! == "15")
        XCTAssert(queryItems[2].name == "location")
        XCTAssert(queryItems[2].value! == "Genoa")
    }
    
    func assertIsCorrect(business: Business) {
        XCTAssert(business.id == "zHkX8xrmOsBx0G7BIPYoNw")
        XCTAssert(business.alias == "ians-pizza-denver-2")
        XCTAssert(business.name == "Bob's Donuts")
        XCTAssert(business.imageURL == "https://s3-media1.fl.yelpcdn.com/bphoto/0w67W4f2a1rIDknkgJrNoQ/o.jpg")
        XCTAssert(business.isClosed == false)
        XCTAssert(business.url == "https://www.yelp.com/biz/ians-pizza-denver-2?adjust_creative=BpFVPV11ZFE2W-sX9FaOUw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=BpFVPV11ZFE2W-sX9FaOUw")
        XCTAssert(business.reviewCount == 420)
        XCTAssert(business.categories == [Category(alias: "pizza", title: "Pizza")])
        XCTAssert(business.rating == 4.5)
        XCTAssert(business.coordinates == Coordinate(coordinates: (39.7562312792762, -104.99171739106)))
        XCTAssert(business.location?.address1 == "2210 Blake St")
        XCTAssert(business.location?.address2 == "")
        XCTAssert(business.location?.address3 == "")
        XCTAssert(business.location?.city == "Denver")
        XCTAssert(business.location?.zipCode == "80205")
        XCTAssert(business.location?.country == "US")
        XCTAssert(business.location?.state == "CO")
        XCTAssert(business.location?.displayAddress[0] == "2210 Blake St")
        XCTAssert(business.location?.displayAddress[1] == "Denver, CO 80205")
        XCTAssert(business.phone == "+13032969000")
        XCTAssert(business.displayPhone == "(303) 296-9000")
        XCTAssert(business.distance == 2944.7712219695445)
    }
    
    func defaultBusinessesResult(name: String) -> String {
        return "{\"businesses\": [\(defaultBusiness(name: name))], \"total\": 1700, \"region\": {\"center\": {\"longitude\": -104.97024536132812,\"latitude\": 39.73552421220229}}}"
    }
    
    func defaultBusiness(name: String) -> String {
        return "{\"id\": \"zHkX8xrmOsBx0G7BIPYoNw\", \"alias\": \"ians-pizza-denver-2\", \"name\": \"\(name)\", \"image_url\": \"https://s3-media1.fl.yelpcdn.com/bphoto/0w67W4f2a1rIDknkgJrNoQ/o.jpg\", \"is_closed\": false, \"url\": \"https://www.yelp.com/biz/ians-pizza-denver-2?adjust_creative=BpFVPV11ZFE2W-sX9FaOUw&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=BpFVPV11ZFE2W-sX9FaOUw\", \"review_count\": 420, \"categories\": [{\"alias\": \"pizza\", \"title\": \"Pizza\"}], \"rating\": 4.5, \"coordinates\": {\"latitude\": 39.7562312792762, \"longitude\": -104.99171739106}, \"transactions\": [\"pickup\", \"delivery\"], \"price\": \"$\", \"location\": {\"address1\": \"2210 Blake St\", \"address2\": \"\", \"address3\": \"\", \"city\": \"Denver\", \"zip_code\": \"80205\", \"country\": \"US\", \"state\": \"CO\", \"display_address\": [\"2210 Blake St\", \"Denver, CO 80205\"]}, \"phone\": \"+13032969000\", \"display_phone\": \"(303) 296-9000\", \"distance\": 2944.7712219695445}"
    }
}
