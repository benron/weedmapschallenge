//
//  Copyright © 2018 Weedmaps, LLC. All rights reserved.
//

import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    // MARK: Properties
    
    var window: UIWindow?

    // MARK: Lifecycle
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        bootstrapServices(with: launchOptions)
        
        return true
    }
    
    private func bootstrapServices(with launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else {
                return
            }
            for service in strongSelf.services {
                service.launch(with: launchOptions)
            }
        }
    }
    
    lazy var services: [BootstrapService] = {
        return [CacheConfigurationService()]
    }()
}

protocol BootstrapService {
    func launch(with launchOptions: [UIApplication.LaunchOptionsKey: Any]?)
}


struct CacheConfigurationService: BootstrapService {
    
    func launch(with launchOptions: [UIApplication.LaunchOptionsKey : Any]?) {
        let cacheSizeMemory = 500*1024*1024
        let cacheSizeDisk = 500*1024*1024
        let sharedCache = URLCache(memoryCapacity: cacheSizeMemory, diskCapacity: cacheSizeDisk, diskPath: "weedmapscache")
        URLCache.shared = sharedCache
    }
    
}
