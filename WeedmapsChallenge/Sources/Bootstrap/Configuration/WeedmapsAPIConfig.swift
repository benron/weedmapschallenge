//
//  APIConfig.swift
//  WeedmapsChallenge
//
//  Created by Benjamin Pilcher on 3/1/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import WeedmapsCore

// Maps a scheme name (capitalized by convention) to
// an environment name that makes sense for development.
// Could also have staging, beta, etc
enum Environment: String {
    case development
    case integration
    case production
    
    init?(fromRawValue: String) {
        guard let environment = Environment(rawValue: fromRawValue.lowercased()) else {
            return nil
        }
        self = environment
    }
}

class WeedmapsAPIConfig: APIConfig {
    static let sharedInstance = WeedmapsAPIConfig()
    
    let environment: Environment
    let baseURLPath: String
    
    private let filename = "Config"
    private let filetype = "plist"
    
    init() {
        let (environment, baseURL) = ProjectConfiguration.config(at: Bundle.main.path(forResource: "Config", ofType: "plist") ?? "")
        
        self.environment = environment
        self.baseURLPath = baseURL
        
        print("Environment: \(environment), base url for webservice: \(baseURLPath)")
    }
    
}

fileprivate enum ProjectConfiguration {
    static func config(at path: String, bundle: Bundle = .main) -> (environment: Environment, baseURL: String) {
        // todo: good to fail early, but is this what we want? also todo: improve string literal usage
        // also it's feasible to write tests on the plists themselves, if not common. That
        // would get rid of some risk here.
        guard let currentConfig = bundle.object(forInfoDictionaryKey: "Config") as? String else {
            fatalError()
        }
        
        guard let environment = Environment(fromRawValue: currentConfig) else {
            fatalError()
        }
        
        // get dictionary
        guard let configurations = NSDictionary(contentsOfFile: (bundle.path(forResource: "Config", ofType: "plist") ?? "")) else {
            fatalError()
        }
        
        let dict = configurations.object(forKey: currentConfig) as! NSDictionary
        guard let urlPath = dict.object(forKey: "BaseURL") as? String else { // todo: make enum of keys?
            fatalError()
        }
        
        return (environment, urlPath)
    }
}
