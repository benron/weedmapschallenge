//
//  UserLocationViewModel.swift
//  WeedmapsChallenge
//
//  Created by Benjamin Pilcher on 3/6/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationProviding: class {
    static func authorizationStatus() -> CLAuthorizationStatus
    var delegate: CLLocationManagerDelegate? { get set }
    var desiredAccuracy: CLLocationAccuracy { get set }
    var distanceFilter: CLLocationDistance { get set }
    func requestWhenInUseAuthorization()
    func requestLocation()
    func stopUpdatingLocation()
}

extension CLLocationManager: LocationProviding { }

protocol GetLocationViewModelDelegate: class {
    func gotCurrentLocation(location: UserLocation)
    func didFailWithError(_ error: Error)
}

class GetLocationViewModel: ViewModelImplementation<UserLocation> {
    
    let locationManager: LocationProviding
    fileprivate let updateDelegate = LocationUpdateDelegate()
    
    weak var delegate: GetLocationViewModelDelegate?
    
    init(locationManager: LocationProviding = CLLocationManager()) {
        self.locationManager = locationManager
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        self.locationManager.distanceFilter = 30
    }
    
    var isAuthorized: Bool {
        let providing: LocationProviding.Type = type(of: locationManager)
        let authorized = providing.authorizationStatus() == .authorizedWhenInUse
        return authorized
    }
    
    var isInitialized: Bool {
        return updateDelegate.isInitialized && locationManager.delegate == nil
    }
    
    func getCurrentLocation() {
        if !isInitialized {
            initializeUpdateDelegate()
            locationManager.delegate = updateDelegate
        }
        
        if isAuthorized {
            locationManager.requestLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func stopUpdating() {
        locationManager.stopUpdatingLocation()
    }
    
    func initializeUpdateDelegate() {
        updateDelegate.receivedPlacemarks = { placemarks in
            guard self.isAuthorized == true else { return }
            guard let placemark = placemarks?.first else { return }
            
            if let location = UserLocation(placemark: placemark) {
                self.delegate?.gotCurrentLocation(location: location)
                self.updates?([location])
            }
        }
        
        updateDelegate.failWithError = { e in
            self.locationManager.requestLocation()
        }
        
        updateDelegate.changeAuthorization = { status in
            switch status {
            case .denied, .restricted:
                break // todo: handle this edge case
            case .authorizedWhenInUse:
                self.getCurrentLocation()
            default: break
            }
        }
    }
}

final private class LocationUpdateDelegate: NSObject, CLLocationManagerDelegate {
    
    let geocoder = CLGeocoder()
    var receivedPlacemarks: (([CLPlacemark]?) -> Void)?
    var failWithError: ((Error?) -> Void)?
    var changeAuthorization: ((CLAuthorizationStatus) -> Void)?
    
    var isInitialized: Bool {
        return receivedPlacemarks != nil && failWithError != nil && changeAuthorization != nil
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        
        print("found \(locations.count) locations")
        
        geocoder.reverseGeocodeLocation(location) { placemarks, _ in
            assert(Thread.isMainThread)
            print("geocoding done")
            
            self.receivedPlacemarks?(placemarks)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        failWithError?(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        changeAuthorization?(status)
    }
    
}
