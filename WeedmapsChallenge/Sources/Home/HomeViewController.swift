//
//  Copyright © 2018 Weedmaps, LLC. All rights reserved.
//

import UIKit
import WeedmapsCore

class HomeViewController: UIViewController, UICollectionViewDelegateFlowLayout {

    // MARK: Properties
    
    // Dependencies (non-private so that they can be overridden in tests)
    // Ideally I'd create the view controller programmatically so that
    // I could initialize it with the properties below
    let imageCache: ImageCache = PlatformImageCache()
    let viewModel = HomeViewModelImplementation()
    let resultUpdating = SearchResultUpdating()
    
    // Private
    private var searchResults = [Business]()
    private var featured: [Business] = []
    private var local: [Business] = []
    
    private let featuredLabel = UILabel.makeBody()
    private let nearYouLabel = UILabel.makeBody()
    private var searchController: UISearchController!
    private var resultsController = SearchResultsViewController()
    
    // Layout varies depending on horizontal size class, so
    // compact/regular is in terms of that size class
    private var compactConstraints: [NSLayoutConstraint] = []
    private var regularConstraints: [NSLayoutConstraint] = []
    
    @objc
    dynamic private var isLoadingMore = false
    
    private var observations: [NSKeyValueObservation] = []
    
    private(set) lazy var featuredBusinessesCollectionView: UICollectionView = {
        return UICollectionView.makeTopAlignedHorizontalCollectionView()
    }()
    
    private(set) lazy var localBusinessesCollectionView: UICollectionView = {
        return UICollectionView.makeVerticalCollectionView()
    }()
    
    private var localBusinessesLayout: UICollectionViewFlowLayout {
        return localBusinessesCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
    }
    
    lazy var measurementCell: BusinessCell = {
        return Bundle.main.loadNibNamed(BusinessCell.ReuseIdentifier, owner: self, options: nil)?[0] as! BusinessCell
    }()

    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        createBindings()
        
        viewModel.updates = { [weak self] updates in
            guard let strongSelf = self, let update = updates.first else { return }
            
            strongSelf.displayCombinedResults((featured: update.featured?.businesses ?? [], local: update.local?.businesses ?? []))
        }
     
        reloadCollectionViews()
        [featuredLabel, nearYouLabel].forEach { $0.isHidden = true }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateConstraints(for: traitCollection)
    }
  
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewModel.loadResults()
    }
    
    private func configureView() {
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        
        featuredLabel.text = NSLocalizedString("Featured", comment: "")
        nearYouLabel.text = NSLocalizedString("Near you", comment: "")
        featuredLabel.textColor = .white
        nearYouLabel.textColor = .white
        
        view.addSubview(featuredLabel)
        view.addSubview(nearYouLabel)
        view.addSubview(featuredBusinessesCollectionView)
        view.addSubview(localBusinessesCollectionView)
        
        NSLayoutConstraint.activate(makeFeaturedLabelConstraints())
        NSLayoutConstraint.activate(makeLocalLabelConstraints())
        NSLayoutConstraint.activate(makeFeaturedBusinessesCollectionViewConstraints())
        NSLayoutConstraint.activate(makeLocalBusinessSharedConstraints())
        
        compactConstraints.append(contentsOf: makeLocalBusinessesCollectionViewCompactConstraints())
        regularConstraints.append(contentsOf: makeLocalBusinessesCollectionViewRegularConstraints())
        
        [featuredBusinessesCollectionView, localBusinessesCollectionView].forEach {
            $0.delegate = self
            $0.dataSource = self
            $0.backgroundColor = .clear
        }
        
        let cellIdentifier = BusinessCell.ReuseIdentifier
        featuredBusinessesCollectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        localBusinessesCollectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        localBusinessesCollectionView.register(LoadingView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: LoadingView.ReuseIdentifier)
        localBusinessesLayout.footerReferenceSize = CGSize(width: localBusinessesCollectionView.bounds.width, height: Layout.Home.loadingFooterHeight)
        
        configureSearch()
    }
    
    private func configureSearch() {
        searchController = UISearchController(searchResultsController: resultsController)
        searchController.searchResultsUpdater = resultUpdating
        searchController.searchBar.placeholder = NSLocalizedString("Search", comment: "")
        searchController.obscuresBackgroundDuringPresentation = true
        definesPresentationContext = true
        
        resultsController.searchOn = { [unowned self] term in
            self.searchController.searchBar.text = term
            self.resultUpdating.updateSearchResults(for: self.searchController)
        }
        
        searchController.searchBar.sizeToFit()
        
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = true
    }
    
    private func createBindings() {
        let observation = observe(\HomeViewController.isLoadingMore, options: [.new]) { [weak self] _, change in
            guard let newValue = change.newValue, newValue else { return }
            
            self?.getLocalBusinesses(wantsAdditional: true)
        }
        observations.append(observation)
    }
    
    // MARK: Data loading
    
    private func getFeaturedBusinesses() {
        viewModel.getFeaturedResults { [weak self] result in
            switch result {
            case .success(let result):
                self?.displayFeatured(businesses: result.businesses)
            case .failure(let error):
                self?.displayError(error)
            }
        }
    }
    
    private func getLocalBusinesses(wantsAdditional: Bool) {
        viewModel.getResults(for: nil, wantsAdditional: wantsAdditional) { [weak self] result in
            switch result {
            case .success(let result):
                self?.displayLocal(businesses: result.businesses, isPaged: result.isPaged)
            case .failure(let error):
                self?.displayError(error)
            }
        }
    }
    
    private func displayFeatured(businesses: [Business]) {
        featured = businesses
        featuredBusinessesCollectionView.reloadData()
    }
    
    private func displayLocal(businesses: [Business], isPaged: Bool) {
        if isPaged {
            addPage(businesses)
        } else {
            local = businesses
        }
        localBusinessesCollectionView.reloadData()
        resultsController.searchResults = local
    }
    
    private func displayCombinedResults(_ businesses: (featured: [Business], local: [Business])) {
        displayFeatured(businesses: businesses.featured)
        displayLocal(businesses: businesses.local, isPaged: false)
        
        [featuredLabel, nearYouLabel].forEach { $0.isHidden = false; $0.alpha = 0.0 }
        UIView.animate(withDuration: 0.3) {
            [self.featuredLabel, self.nearYouLabel].forEach { $0.alpha = 1.0 }
        }
    }
    
    private func addPage(_ businesses: [Business]) {
        localBusinessesCollectionView.performBatchUpdates({ () -> Void in
            
            let offset = self.local.count
            let rows = [Int](offset...(offset + businesses.count - 1))
            let indexPaths = rows.map { IndexPath(row: $0, section: 0)}
            
            self.local.append(contentsOf: businesses)
            self.localBusinessesCollectionView.insertItems(at: indexPaths)
            
        }, completion: { isFinished in
            if isFinished {
                self.isLoadingMore = false
            }
        })
    }
    
    private func displayError(_ error: Error) {
        // todo: pop up an alert with user-friendly text
    }
    
    private func reloadCollectionViews() {
        [featuredBusinessesCollectionView, localBusinessesCollectionView].forEach { $0.reloadData() }
    }
    
    deinit {
        for element in observations {
            element.invalidate()
        }
    }
    
    // MARK: - Layout
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        viewModel.cancelRequests()
        localBusinessesLayout.invalidateLayout()
        localBusinessesCollectionView.reloadData()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        updateConstraints(for: traitCollection)
        
        var localBusinessesInset = UIEdgeInsets(top: Layout.Home.padding, left: Layout.Home.padding, bottom: Layout.Home.padding, right: Layout.Home.padding)
        
        if view.traitCollection.horizontalSizeClass == .regular {
            let shouldHideFeatured = traitCollection.userInterfaceIdiom == .phone
            featuredHeight.constant = 0.0
            featuredLabel.isHidden = shouldHideFeatured
        } else {
            // compact
            featuredBusinessesCollectionView.isHidden = false
            localBusinessesInset.top = 0.0
            featuredHeight.constant = featuredCellHeight(text: "Foo\nbar\nbiz") // todo: cache value so it's not reevaluated everytime
            featuredLabel.isHidden = featured.isEmpty ? true : false
        }
        
        localBusinessesLayout.sectionInset = localBusinessesInset

        reloadCollectionViews()
    }
    
    private func updateConstraints(for traitCollection: UITraitCollection) {
        let otherConstraints = inactiveConstraints(for: traitCollection)
        if otherConstraints.first?.isActive ?? false {
            NSLayoutConstraint.deactivate(otherConstraints)
        }
        
        let activeConstraints = constraints(for: traitCollection)
        if !(activeConstraints.first?.isActive ?? false) {
            NSLayoutConstraint.activate(activeConstraints)
        }
    }
    private func constraints(for traitCollection: UITraitCollection) -> [NSLayoutConstraint] {
        if traitCollection.userInterfaceIdiom == .pad {
            return compactConstraints
        } else {
            switch traitCollection.horizontalSizeClass {
            case .compact:
                return compactConstraints
            case .regular:
                return regularConstraints
            case .unspecified:
                return []
            }
        }
    }
    
    private func inactiveConstraints(for traitCollection: UITraitCollection) -> [NSLayoutConstraint] {
        if traitCollection.userInterfaceIdiom == .pad {
            return []
        } else {
            switch traitCollection.horizontalSizeClass {
            case .compact:
                return regularConstraints
            case .regular:
                return compactConstraints
            case .unspecified:
                return []
            }
        }
    }
    
    private func makeFeaturedLabelConstraints() -> [NSLayoutConstraint] {
        return [
            featuredLabel.bottomAnchor.constraint(equalTo: featuredBusinessesCollectionView.topAnchor, constant: -Layout.Home.inset),
            featuredLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.Home.padding),
        ]
    }
    
    private func makeLocalLabelConstraints() -> [NSLayoutConstraint] {
        return [
            nearYouLabel.bottomAnchor.constraint(equalTo: localBusinessesCollectionView.topAnchor, constant: -Layout.Home.inset),
            nearYouLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.Home.padding)
        ]
    }
    
    private var featuredHeight: NSLayoutConstraint!
    private func makeFeaturedBusinessesCollectionViewConstraints() -> [NSLayoutConstraint] {
        let constraints = [
            featuredBusinessesCollectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 38.0),
            featuredBusinessesCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            featuredBusinessesCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            featuredBusinessesCollectionView.heightAnchor.constraint(equalToConstant: featuredCellHeight(text: "Foo\nbar\nbiz"))
        ]
        
        featuredHeight = constraints.last!
        
        return constraints
    }
    
    private func makeLocalBusinessSharedConstraints() -> [NSLayoutConstraint] {
        return [
            localBusinessesCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            localBusinessesCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            localBusinessesCollectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ]
    }
    
    private func makeLocalBusinessesCollectionViewCompactConstraints() -> [NSLayoutConstraint] {
        return [
            localBusinessesCollectionView.topAnchor.constraint(equalTo: featuredBusinessesCollectionView.bottomAnchor, constant: 38.0)]
    }
    
    private func makeLocalBusinessesCollectionViewRegularConstraints() -> [NSLayoutConstraint] {
        return [
            localBusinessesCollectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 38.0)]
    }
}

// MARK: UICollectionViewDelegate

extension HomeViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard collectionView == localBusinessesCollectionView else { return UICollectionReusableView() }
        
        if kind == UICollectionView.elementKindSectionFooter {
            return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: LoadingView.ReuseIdentifier, for: indexPath as IndexPath)
        }
        
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        guard collectionView == localBusinessesCollectionView else { return .zero }
        
        let width = localBusinessesCollectionView.bounds.size.width
        return CGSize(width: width, height: Layout.Home.loadingFooterHeight)
    }
    
    var featuredCellWidth: CGFloat {
        let contextWidth = UIScreen.main.bounds.size.width
        let availableWidth = (contextWidth - (Layout.Home.padding)) / 2
        return availableWidth
    }
    
    func featuredCellHeight(text: String) -> CGFloat {
        let availableWidth = featuredCellWidth
        let textHeight = text.textSize(for: UIFont.preferredFont(forTextStyle: .caption1), maxSize: CGSize(width: availableWidth, height: Layout.Home.cellHeight)).height
        let nonTextRelatedHeight = measurementCell.nonTextHeight
        return textHeight + nonTextRelatedHeight
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == featuredBusinessesCollectionView {
            let business = featured[indexPath.row]
            let estimatedSize = CGSize(width: featuredCellWidth, height: featuredCellHeight(text: business.name))
            return estimatedSize
        } else {
            let contextWidth = collectionView.bounds.size.width
            
            let availableWidth = (contextWidth - (Layout.Home.padding * 3)) / 3
            let estimatedSize = CGSize(width: availableWidth - 3, height: Layout.Home.cellHeight * 0.9)
            
            return estimatedSize
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        // todo: store the image url being loaded on the cell as a property, and if it's different
        // than the model's image url return from businesses(for: collectionView)[indexPath.row], cancel the image load.
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // IMPLEMENT:
        // 1a) Present the user with a UIAlertController (action sheet style) with options
        // to either display the Business's Yelp page in a WKWebView OR bump the user out to
        // Safari. Both options should display the Business's Yelp page details
      
        //viewDetails(for: Business(from: ))  // ⚠️ sorry! didn't have time
    }
    
    private func viewDetails(for business: Business) {
        let alert = UIAlertController(title: business.name, message: nil, preferredStyle: .alert)
        
        // todo: localize strings
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in })
        
        let safari = UIAlertAction(title: "View in Safari", style: .default, handler: { _ in
            // todo: pass url to safari
        })

        let inApp = UIAlertAction(title: "See details", style: .default, handler: { _ in
            // todo: create and present HomeDetailVC (better if a higher level object i.e a coordinator did this)
        })
        
        alert.addAction(inApp)
        alert.addAction(safari)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: { })
    }
}

// MARK: UICollectionViewDataSource

extension HomeViewController: UICollectionViewDataSource {
    
    func businesses(for collectionView: UICollectionView) -> [Business] {
        return collectionView == localBusinessesCollectionView ? local : featured
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return businesses(for: collectionView).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BusinessCell.ReuseIdentifier, for: indexPath)
        
        guard let businessCell = cell as? BusinessCell else { return cell }
        
        let business = businesses(for: collectionView)[indexPath.row]
        businessCell.business = business
        
        imageCache.getImage(at: business.imageURL) { result in
            switch result {
            case .success(let image):
                businessCell.image = image
            case .failure:
                // todo: log error to analytics (Firebase, etc)
                businessCell.image = nil
            }
        }
        
        return businessCell
    }
}

extension HomeViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView == localBusinessesCollectionView else { return }
        
        if scrollView.isAtBottom && !isLoadingMore {
            isLoadingMore = true
        }
    }
}
