//
//  HomeViewViewModel.swift
//  WeedmapsChallenge
//
//  Created by Benjamin Pilcher on 3/1/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//import Foundation

import Foundation
import WeedmapsCore

struct BusinessesSearchResult {
    let businesses: [Business]
    let isPaged: Bool
}

typealias BusinessResultsHandler = ((Result<BusinessesSearchResult>) -> Void)

//protocol HomeViewModel: ViewModel {
//    var isDone: Bool { get }
//    func reset()
//    func cancelRequests()
//    func getFeaturedResults(completion: @escaping BusinessResultsHandler)
//    func getResults(for searchTerm: String?, wantsAdditional: Bool, completion: @escaping BusinessResultsHandler)
//}

class HomeViewModelImplementation: ViewModel {
    
    typealias Updates = (featured: BusinessesSearchResult?, local: BusinessesSearchResult?, SearchLocation)
    
    var updates: (([Updates]) -> Void)?
    var event: ViewEvent?
    
    typealias model = Updates
    
    private var limit = 15
    private var offset = 0
    private let queue: DispatchQueue = .main
    private let dispatchGroup: DispatchGroup = DispatchGroup()
    
    private let service = YelpService(config: WeedmapsAPIConfig())
    private var defaultLocation = SearchLocation.location(value: "Denver")
    private(set) var location: SearchLocation?
    
    private var currentRequest: DispatchWorkItem?
    private var lastFire: TimeInterval = 0
    private let delay: TimeInterval = 1.0
    private var getLocationViewModel = GetLocationViewModel()
    private var latestFeaturedResult: BusinessesSearchResult?
    private var latestLocalResult: BusinessesSearchResult?
    
    var isDone: Bool {
        return currentRequest == nil
    }
        
    init() {
        getLocationViewModel.updates = { [unowned self] locations in
            
            guard !locations.isEmpty else {
                return
            }
            
            let coordinates = locations[0].coordinate
            let detectedLocation = SearchLocation.coordinates(lat: coordinates.latitude, lon: coordinates.longitude)
            self.location = detectedLocation
            
            self.loadCombinedResults()
        }
    }
    
    func loadResults() {
        reset()
        
        attemptToGetLocation { _ in
            self.loadCombinedResults()
        }
    }

    private func loadCombinedResults() {
        dispatchGroup.enter()
        
        self.getFeaturedResults(completion: { [weak self]result in
            self?.dispatchGroup.leave()
        })
        
        dispatchGroup.enter()
        self.getResults(for: nil, wantsAdditional: false) { [weak self] result in
            self?.dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: queue) { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.updates?([strongSelf.buildUpdates()])
        }
    }
    
    func attemptToGetLocation(completion: @escaping (Result<Bool>) -> ()) {
        guard location == nil else { return }
        
        getLocationViewModel.getCurrentLocation()
    }
    
    private func buildUpdates() -> Updates {
        return (latestFeaturedResult, latestLocalResult, location ?? defaultLocation)
    }
    
    func reset() {
        cancelRequests()
        offset = 0
    }

    func cancelRequests() {
        currentRequest?.cancel()
    }
    
    func getFeaturedResults(completion: @escaping BusinessResultsHandler) {
        self.service.search(for: nil, searchLocation: self.location ?? defaultLocation, offset: self.offset, limit: self.limit, attributes: ["hot_and_new"]) { [weak self] result in
            guard let strongSelf = self else { return }
            
            let keypath = \HomeViewModelImplementation.latestFeaturedResult
            strongSelf.handleResult(result, wantsAdditional: false, keyPath: keypath, completion: completion)
        }
    }

    func getResults(for searchTerm: String?, wantsAdditional: Bool, completion: @escaping BusinessResultsHandler) {
        if wantsAdditional {
            offset += limit
        }
        
        let newSearchRequest = DispatchWorkItem { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.service.search(for: searchTerm, searchLocation: strongSelf.location ?? strongSelf.defaultLocation, offset: strongSelf.offset, limit: strongSelf.limit) { [weak self] result in
                guard let strongSelf = self else { return }
                
                let keypath = \HomeViewModelImplementation.latestLocalResult
                strongSelf.handleResult(result, wantsAdditional: wantsAdditional, keyPath: keypath, completion: completion)
            }
            
            strongSelf.lastFire = Date().timeIntervalSinceReferenceDate
            strongSelf.currentRequest = nil
        }
        
        delay.hasPassed(lastFire) ? DispatchQueue.main.async(execute: newSearchRequest) : DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: newSearchRequest)
        currentRequest = newSearchRequest
    }
    
    private func handleResult(_ result: Result<[Business]>, wantsAdditional: Bool, keyPath: ReferenceWritableKeyPath<HomeViewModelImplementation, BusinessesSearchResult?>, completion: @escaping BusinessResultsHandler) {
        
        switch result {
        case .success(let businesses):
            var page = businesses
            if wantsAdditional {
                var current = self[keyPath: keyPath]?.businesses ?? []
                current.append(contentsOf: businesses)
                page = current
            } else {
                page = businesses
            }
            let searchResult = BusinessesSearchResult(businesses: page, isPaged: wantsAdditional)
            self[keyPath: keyPath] = searchResult
            enqueueResult(.success(BusinessesSearchResult(businesses: businesses, isPaged: wantsAdditional)), handler: completion)
        case .failure(let error):
            enqueueResult(.failure(error), handler: completion)
        }
    }
    
    private func enqueueResult(_ result: Result<BusinessesSearchResult>, handler: @escaping BusinessResultsHandler) {
        queue.async() {
            handler(result)
        }
    }
}
