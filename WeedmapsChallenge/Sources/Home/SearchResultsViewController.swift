//
//  Search.swift
//  WeedmapsChallenge
//
//  Created by Benjamin Pilcher on 3/3/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import UIKit
import WeedmapsCore

protocol Searchable {
    func update(text: String?)
}

class SearchResultUpdating: NSObject, UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchable = searchController.searchResultsController as? Searchable else {
            return
        }
        searchable.update(text: searchController.searchBar.text)
    }
}

class SearchResultsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, Searchable {
    
    var searchOn: ((String) -> ())?
    
    // Dependencies
    let viewModel = HomeViewModelImplementation()
    let imageCache: ImageCache = PlatformImageCache()
    let searchHistoryStore: SearchHistoryStore = SearchHistoryStoreImplementation()
    
    private(set) lazy var searchHistoryCollectionView: UICollectionView = {
        return UICollectionView.makeTopAlignedHorizontalCollectionView()
    }()
    
    private(set) lazy var collectionView: UICollectionView = {
        return UICollectionView.makeVerticalCollectionView()
    }()
    
    var searchResults: [Business] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    
    var searchHistory: SearchHistory?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(collectionView)
        view.addSubview(searchHistoryCollectionView)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        searchHistoryCollectionView.delegate = self
        searchHistoryCollectionView.dataSource = self
        collectionView.contentInset = UIEdgeInsets(top: 0.0, left: 8.0, bottom: 8.0, right: 8.0)
        
        let cellIdentifier = BusinessCell.ReuseIdentifier
        collectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)

        NSLayoutConstraint.activate(makeSearchCollectionViewConstraints())
        NSLayoutConstraint.activate(makeCollectionViewConstraints())
        
        searchHistoryCollectionView.register(UINib(nibName: SearchTermCell.ReuseIdentifier, bundle: nil), forCellWithReuseIdentifier: SearchTermCell.ReuseIdentifier)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        loadSearchHistory()
    }
    
    private func loadSearchHistory() {
        searchHistoryStore.getSearchHistory { [weak self] history in
            guard let strongSelf = self else { return }
            
            if let previousActivity = history {
                strongSelf.searchHistory = previousActivity
                strongSelf.searchHistoryCollectionView.reloadData()
            }
        }
    }
    private func makeSearchCollectionViewConstraints() -> [NSLayoutConstraint] {
        return [
            searchHistoryCollectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            searchHistoryCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            searchHistoryCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            searchHistoryCollectionView.heightAnchor.constraint(equalToConstant: 40.0)
        ]
    }
    
    private func makeCollectionViewConstraints() -> [NSLayoutConstraint] {
        return [
            collectionView.topAnchor.constraint(equalTo: searchHistoryCollectionView.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)        ]
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BusinessCell.ReuseIdentifier, for: indexPath)
            configure(cell as! BusinessCell, at: indexPath)
            configure(cell as! BusinessCell, at: indexPath)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchTermCell.ReuseIdentifier, for: indexPath)
            configure(cell as! SearchTermCell, at: indexPath)
            return cell
        }
    }
    
    private func configure(_ businessCell: BusinessCell, at indexPath: IndexPath) {
        let business = searchResults[indexPath.row]
        businessCell.business = business
        
        imageCache.getImage(at: business.imageURL) { result in
            switch result {
            case .success(let image):
                businessCell.image = image
            case .failure(let error):
                print("\(error)")
            }
        }
    }
    
    private func configure(_ cell: SearchTermCell, at indexPath: IndexPath) {
        let term = searchHistory?.terms.sorted()[indexPath.row]
        cell.term = term
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionView {
            return searchResults.count
        } else {
            return searchHistory?.terms.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collectionView {
            let contextWidth = collectionView.bounds.size.width
            let availableWidth = (contextWidth - (Layout.Home.padding * 3)) / 3
            let estimatedSize = CGSize(width: availableWidth - 3, height: Layout.Home.cellHeight * 0.85)
            return estimatedSize
        } else {
            let font = UIFont.preferredFont(forTextStyle: .caption1)
            let text = searchHistory?.terms[indexPath.row] ?? ""
            let size = text.textSize(for: font, maxSize: CGSize(width: 80.0, height: 40.0))
            return CGSize(width: size.width + 30.0, height: 40.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == searchHistoryCollectionView {
            let searchTerm = searchHistory?.terms.sorted()[indexPath.row] ?? ""
            searchOn?(searchTerm)
        }
    }
    
    func update(text: String?) {
        guard let searchTerm = text, !searchTerm.isEmpty else {
            viewModel.cancelRequests()
            return
        }
        
        viewModel.getResults(for: searchTerm, wantsAdditional: false) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let result):
                if result.isPaged {
                    strongSelf.searchResults.append(contentsOf: result.businesses)
                } else {
                    strongSelf.searchResults = result.businesses
                }
                
                if strongSelf.viewModel.isDone  {
                    self?.saveSearchTerm(searchTerm)
                }
            case .failure:
                print("error")
            }
        }
    }
    
    private func saveSearchTerm(_ term: String) {
        searchHistory?.appendTerm(term)
        searchHistoryCollectionView.reloadData()
        searchHistoryStore.saveSearchHistory(searchHistory!)
    }
}
