//
//  ViewModel.swift
//  WeedmapsChallenge
//
//  Created by Benjamin Pilcher on 3/6/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import Foundation

enum ViewEvent { case didLoad, willAppear, willDisappear }

protocol ViewModel: class {
    associatedtype model
    var updates: (([model]) -> Void)? { get set }
    var event: ViewEvent? { get set }
}

class ViewModelImplementation<T>: NSObject, ViewModel {

    var updates: (([T]) -> Void)?
    var event: ViewEvent?
}
