//
//  SearchHistory.swift
//  WeedmapsChallenge
//
//  Created by Benjamin Pilcher on 3/4/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import Foundation

protocol SearchHistoryStore {
    func saveSearchHistory(_ searchHistory: SearchHistory)
    func getSearchHistory(completion: @escaping (SearchHistory?) -> ())
}

class SearchHistoryStoreImplementation: SearchHistoryStore {
    
    func getDocumentsURL() -> URL {
        if let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            return url
        } else {
            fatalError("Could not retrieve documents directory")
        }
    }
    
    func saveSearchHistory(_ searchHistory: SearchHistory) {
        let url = getDocumentsURL().appendingPathComponent("searchHistory.json")
        
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(searchHistory)
            try data.write(to: url, options: [])
        } catch {
            fatalError(error.localizedDescription)
        }
    }
    
    func getSearchHistory(completion: @escaping (SearchHistory?) -> ()) {
        DispatchQueue.global(qos: .userInteractive).async { [weak self] in
            guard let strongSelf = self else { return }
            
            let url = strongSelf.getDocumentsURL().appendingPathComponent("searchHistory.json")
            let decoder = JSONDecoder()
            do {
                let data = try Data(contentsOf: url, options: [])
                let searchHistory = try decoder.decode(SearchHistory.self, from: data)
                
                DispatchQueue.main.async {
                    completion(searchHistory)
                }
            } catch {
                DispatchQueue.main.async {
                    completion(SearchHistory(terms: []))
                }
            }
        }
    }
}

struct SearchHistory: Codable {
    var terms: [String]
    
    mutating func appendTerm(_ term: String) {
        guard term.count > 1 else { return }
        
        // I make no claims about the performance of this algorithm,
        // I'd need a little more time to measure its performance, investigate some better options.
        var toRemove: Set<String> = []
        for element in terms {
            if term.lowercased().range(of: element.lowercased()) != nil || element.lowercased().range(of: term.lowercased()) != nil {
                toRemove.insert(element)
            }
        }
        
        terms.append(term)
        terms = Array(Set(terms).subtracting(toRemove))
    }
}
