//
//  UIScrollview+Util.swift
//  WeedmapsChallenge
//
//  Created by Benjamin Pilcher on 3/4/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import UIKit

extension UIScrollView {
    var isAtBottom: Bool {
        let scrollOffset = contentOffset.y
        guard scrollOffset > 0 else {
            return false
        }
        
        let viewHeight = bounds.size.height
        let contentHeight = contentSize.height
        
        return viewHeight + scrollOffset == contentHeight
    }
}
