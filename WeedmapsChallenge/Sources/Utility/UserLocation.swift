//
//  UserLocation.swift
//  WeedmapsChallenge
//
//  Created by Benjamin Pilcher on 3/6/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import Foundation
import CoreLocation

public class UserLocation: Codable {
    
    public let city: String
    public let latitude: Double
    public let longitude: Double
    
    private static let separator = "_"
    
    enum CodingKeys: String, CodingKey {
        case city = "city"
        case latitude = "latitude"
        case longitude = "longitude"
    }
    
    public init?(placemark: CLPlacemark) {
        guard let city = placemark.locality ?? placemark.name, !city.isEmpty else {
            print("Couldn't derive city")
            return nil
        }
        
        guard let lat = placemark.location?.coordinate.latitude, let lon = placemark.location?.coordinate.longitude else {
            print("Couldn't derive coordinates")
            return nil
        }
        
        self.city = city
        self.latitude = lat
        self.longitude = lon
    }
    
    public var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}

extension UserLocation: Equatable {
    public static func == (lhs: UserLocation, rhs: UserLocation) -> Bool {
        return lhs.city == rhs.city && lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
    }
}
