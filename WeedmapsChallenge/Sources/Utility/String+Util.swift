//
//  String+Util.swift
//  WeedmapsChallenge
//
//  Created by Benjamin Pilcher on 3/4/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import UIKit

extension String {
    public func textSize(for font: UIFont, maxSize: CGSize) -> CGSize {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = -2.0
        let attrString = NSAttributedString(string: self, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.paragraphStyle: paragraphStyle])
        let options: NSStringDrawingOptions = NSStringDrawingOptions.usesLineFragmentOrigin
        let rect = attrString.boundingRect(with: maxSize, options: options, context: nil)
        let size = rect.size
        
        return size
    }
}
