//
//  TopAlignedCollectionViewFlowLayout.swift
//  WeedmapsChallenge
//
//  Created by Benjamin Pilcher on 3/4/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import UIKit

class TopAlignedCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let attributes = super.layoutAttributesForElements(in: rect) else {
            return nil
        }
        var attributesCopy = [UICollectionViewLayoutAttributes]()
        for attr in attributes {
            let attrCopy = attr.copy() as! UICollectionViewLayoutAttributes
            if (attrCopy.representedElementCategory == .cell) {
                attrCopy.frame.origin.y = 0.0
            }
            
            attributesCopy.append(attrCopy)
        }
        
        return attributesCopy
    }
}
