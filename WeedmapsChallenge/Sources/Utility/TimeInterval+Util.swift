//
//  TimeInterval+Util.swift
//  WeedmapsChallenge
//
//  Created by Benjamin Pilcher on 3/4/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import Foundation

extension TimeInterval {
    
    func hasPassed(_ time: TimeInterval) -> Bool {
        return time < Date().timeIntervalSinceReferenceDate - self
    }
}
