//
//  NaiveImageCache.swift
//  WeedmapsChallenge
//
//  Created by Benjamin Pilcher on 3/2/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import UIKit
import WeedmapsCore

protocol ImageCache {
    func getImage(at urlPath: String?, completion: @escaping ((WeedmapsCore.Result<UIImage>) -> ()))
}

class PlatformImageCache: ImageCache {
    
    private let cachePolicy: NSURLRequest.CachePolicy
    private let queue: DispatchQueue = .main
    private let timeoutInterval: TimeInterval = 10
    
    init(cachePolicy: NSURLRequest.CachePolicy = .returnCacheDataElseLoad) {
        self.cachePolicy = cachePolicy
    }
    
    enum InvalidImage: Error {
        case url
        case response(_ urlResponse: URLResponse?)
        case data
    }
    
    func getImage(at urlPath: String?, completion: @escaping ((WeedmapsCore.Result<UIImage>) -> ())) {
        guard let url = urlPath.flatMap(URL.init) else {
            return
        }
        
        let request = NSMutableURLRequest(url: url)
        request.cachePolicy = cachePolicy
        request.timeoutInterval = timeoutInterval
        
        let dataTask = URLSession.shared.dataTask(with: request as URLRequest) { [weak self] data, response, error in
            guard let strongSelf = self else { return }
            
            if let responseError = error {
                strongSelf.finishWithResult(.failure(responseError), completion: completion)
                return
            }
            
            guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200 else {
                strongSelf.finishWithResult(.failure(InvalidImage.response(response)), completion: completion)
                return
            }
            
            guard let imageData = data, let image = UIImage(data: imageData) else {
                strongSelf.finishWithResult(.failure(InvalidImage.data), completion: completion)
                return
            }
        
            
            strongSelf.finishWithResult(.success(image), completion: completion)
        }
        
        dataTask.resume()
    }
    
    func finishWithResult(_ result: WeedmapsCore.Result<UIImage>, completion: @escaping ((WeedmapsCore.Result<UIImage>) -> ())) {
        queue.async() {
            completion(result)
        }
    }
}
