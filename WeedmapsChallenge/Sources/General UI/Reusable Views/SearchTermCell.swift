//
//  Copyright © 2018 Weedmaps, LLC. All rights reserved.
//

import UIKit

class SearchTermCell: UICollectionViewCell {
    
    @IBOutlet private weak var termLabel: UILabel!
    
    static let ReuseIdentifier: String = NSStringFromClass(SearchTermCell.self).withoutModuleName

    override func awakeFromNib() {
        super.awakeFromNib()
        
        termLabel.font = UIFont.preferredFont(forTextStyle: .caption1)
        termLabel.isOpaque = true
        termLabel.backgroundColor = UIColor.white
        termLabel.textColor = UIColor.darkText
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        term = nil
    }
    
    var term: String? {
        didSet {
            termLabel.text = term  ?? ""
        }
    }
}

extension String {
    var withoutModuleName: String {
        return self.components(separatedBy: ".").last ?? ""
    }
}
