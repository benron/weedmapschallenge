//
//  Copyright © 2018 Weedmaps, LLC. All rights reserved.
//

import UIKit
import WeedmapsCore

class BusinessCell: UICollectionViewCell {
    
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    
    static let ReuseIdentifier: String = NSStringFromClass(BusinessCell.self).withoutModuleName

    override func awakeFromNib() {
        super.awakeFromNib()
        
        descriptionLabel.font = UIFont.preferredFont(forTextStyle: .caption1)
        descriptionLabel.isOpaque = true
        descriptionLabel.backgroundColor = .white
 
        round(self.layer, cornerRadius: 8.0)
        round(imageView.layer, cornerRadius: 6.0)
    }
    
    var nonTextHeight: CGFloat {
        let totalDesiredVerticalPadding: CGFloat = 20.0
        return imageHeightConstraint.constant + totalDesiredVerticalPadding
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        business = nil
    }
    
    func round(_ layer: CALayer, cornerRadius: CGFloat) {
        layer.masksToBounds = true
        layer.cornerRadius = cornerRadius
    }
    
    var business: Business? {
        didSet {
            guard let model = business else {
                descriptionLabel.text = ""
                imageView.image = nil
                return
            }
            
            descriptionLabel.text = model.name
        }
    }
    
    var image: UIImage? {
        didSet {
            imageView.image = image
        }
    }
}
