//
//  UICollectionView+Factory.swift
//  WeedmapsChallenge
//
//  Created by Benjamin Pilcher on 3/4/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import UIKit

extension UICollectionView {
    static func makeTopAlignedHorizontalCollectionView() -> UICollectionView  {
        let layout = TopAlignedCollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 8.0
        let view = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
        view.showsHorizontalScrollIndicator = false
        view.backgroundColor = UIColor.white
        view.contentInset = UIEdgeInsets(top: 0.0, left: 8.0, bottom: 0.0, right: 8.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.collectionViewLayout = layout
        return view
    }
    
    static func makeVerticalCollectionView() -> UICollectionView  {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 8.0
        layout.minimumLineSpacing = 8.0
        let view = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
        view.showsVerticalScrollIndicator = false
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.collectionViewLayout = layout
        return view
    }
}
