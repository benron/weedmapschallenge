//
//  UILabel+Factory.swift
//  WeedmapsChallenge
//
//  Created by Benjamin Pilcher on 3/4/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import UIKit

extension UILabel {
    static func makeBody(text: String? = nil, color: UIColor? = nil) -> UILabel {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .left
        view.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)
        view.isOpaque = true
        view.text = text
        view.textColor = UIColor.white

        return view
    }
}
