//
//  Layout.swift
//  WeedmapsChallenge
//
//  Created by Benjamin Pilcher on 3/4/19.
//  Copyright © 2019 Weedmaps, LLC. All rights reserved.
//

import UIKit

enum Layout {
    enum Home {
        static let inset: CGFloat = 10.0
        static let padding: CGFloat = 8.0
        static let lineSpacing: CGFloat = 20.0
        static let loadingFooterHeight: CGFloat = 50.0
        static let cellHeight: CGFloat = 200.0
    }
}
